PDFLATEX = pdflatex -synctex=1 -interaction=nonstopmode
BIB = bibtex

.PHONY: AnnotatedBibliography.pdf all clean

%.tex: %.raw
	./raw2tex $< > $@

%.tex: %.dat
	./dat2tex $< > $@

# MAIN LATEXMK RULE

# -pdf tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make tells latexmk to call make for generating missing files.
# -interactive=nonstopmode keeps the pdflatex backend from stopping at a
# missing file reference and interactively asking you for an alternative.

AnnotatedBibliography.pdf: AnnotatedBibliography.tex
	latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make AnnotatedBibliography.tex

clean:
	latexmk -CA



